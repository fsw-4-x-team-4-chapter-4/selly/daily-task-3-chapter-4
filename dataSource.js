const datasource = 

[
  {
    "_id": "632c7ad32c686246b74923d1",
    "index": 0,
    "age": 20,
    "isActive": false,
    "eyeColor": "blue",
    "name": "Hutchinson Bridges",
    "gender": "male",
    "company": "Pelangi",
    "email": "hutchinsonbridges@telpod.com",
    "phone": "+1 (987) 512-2781",
    "address": "438 Grace Court, Glenshaw, South Carolina, 7616",
    "registered": "2015-03-30T11:30:12 -07:00",
    "favoriteFruit": "strawberry"
  },
  {
    "_id": "632c7ad3eaf23af5802f7fb0",
    "index": 1,
    "age": 22,
    "isActive": true,
    "eyeColor": "blue",
    "name": "Sherrie Mcgowan",
    "gender": "female",
    "company": "FSW4",
    "email": "sherriemcgowan@telpod.com",
    "phone": "+1 (831) 418-3796",
    "address": "925 Heath Place, Brookfield, Kentucky, 8093",
    "registered": "2016-01-01T09:42:05 -07:00",
    "favoriteFruit": "strawberry"
  },
  {
    "_id": "632c7ad3297a8cbb90a02caa",
    "index": 2,
    "age": 30,
    "isActive": true,
    "eyeColor": "blue",
    "name": "Aguilar Duffy",
    "gender": "male",
    "company": "Intel",
    "email": "aguilarduffy@telpod.com",
    "phone": "+1 (820) 512-3258",
    "address": "706 Harwood Place, Coyote, Ohio, 875",
    "registered": "2020-07-11T11:04:38 -07:00",
    "favoriteFruit": "strawberry"
  },
  {
    "_id": "632c7ad30c584ecac57ab43d",
    "index": 3,
    "age": 37,
    "isActive": false,
    "eyeColor": "blue",
    "name": "Dixon Oneil",
    "gender": "male",
    "company": "FSW4",
    "email": "dixononeil@telpod.com",
    "phone": "+1 (818) 539-3205",
    "address": "950 Fountain Avenue, Bowden, Rhode Island, 5384",
    "registered": "2020-02-06T07:03:30 -07:00",
    "favoriteFruit": "strawberry"
  },
  {
    "_id": "632c7ad3667568e826c4f563",
    "index": 4,
    "age": 20,
    "isActive": true,
    "eyeColor": "green",
    "name": "Wright Miles",
    "gender": "male",
    "company": "FSW4",
    "email": "wrightmiles@telpod.com",
    "phone": "+1 (901) 565-2090",
    "address": "531 Beverly Road, Tampico, Wisconsin, 5233",
    "registered": "2020-01-26T09:04:37 -07:00",
    "favoriteFruit": "banana"
  },
  {
    "_id": "632c7ad3eed7fa213807b66f",
    "index": 5,
    "age": 36,
    "isActive": false,
    "eyeColor": "blue",
    "name": "Carpenter Nelson",
    "gender": "male",
    "company": "FSW4",
    "email": "carpenternelson@telpod.com",
    "phone": "+1 (839) 530-3102",
    "address": "673 Jardine Place, Wright, American Samoa, 3966",
    "registered": "2021-11-17T09:59:41 -07:00",
    "favoriteFruit": "apple"
  },
  {
    "_id": "632c7ad38e0f165e109ec6f7",
    "index": 6,
    "age": 20,
    "isActive": false,
    "eyeColor": "brown",
    "name": "Melisa Sandoval",
    "gender": "female",
    "company": "FSW4",
    "email": "melisasandoval@telpod.com",
    "phone": "+1 (880) 433-2596",
    "address": "829 Grafton Street, Goldfield, Wyoming, 5859",
    "registered": "2017-06-24T02:10:21 -07:00",
    "favoriteFruit": "strawberry"
  },
  {
    "_id": "632c7ad32099b469129314d9",
    "index": 7,
    "age": 32,
    "isActive": true,
    "eyeColor": "green",
    "name": "Farrell Juarez",
    "gender": "male",
    "company": "Pelangi",
    "email": "farrelljuarez@telpod.com",
    "phone": "+1 (909) 559-3480",
    "address": "335 Estate Road, Chestnut, Marshall Islands, 1064",
    "registered": "2019-10-19T10:09:54 -07:00",
    "favoriteFruit": "strawberry"
  },
  {
    "_id": "632c7ad357b2d16e612a2edd",
    "index": 8,
    "age": 29,
    "isActive": true,
    "eyeColor": "blue",
    "name": "Patterson Young",
    "gender": "male",
    "company": "FSW4",
    "email": "pattersonyoung@telpod.com",
    "phone": "+1 (871) 510-2768",
    "address": "221 Sharon Street, Guilford, Virginia, 7146",
    "registered": "2019-07-03T10:50:37 -07:00",
    "favoriteFruit": "banana"
  },
  {
    "_id": "632c7ad3714a38f3fab8d2aa",
    "index": 9,
    "age": 34,
    "isActive": true,
    "eyeColor": "brown",
    "name": "Katy Sloan",
    "gender": "female",
    "company": "Pelangi",
    "email": "katysloan@telpod.com",
    "phone": "+1 (904) 594-3740",
    "address": "834 Woodhull Street, Bowmansville, Federated States Of Micronesia, 1118",
    "registered": "2016-06-22T09:48:55 -07:00",
    "favoriteFruit": "apple"
  },
  {
    "_id": "632c7ad3ff5e02afe361bb65",
    "index": 10,
    "age": 32,
    "isActive": true,
    "eyeColor": "green",
    "name": "Spears Booker",
    "gender": "male",
    "company": "FSW4",
    "email": "spearsbooker@telpod.com",
    "phone": "+1 (896) 421-2767",
    "address": "358 Carroll Street, Valle, Illinois, 4517",
    "registered": "2016-10-01T04:33:16 -07:00",
    "favoriteFruit": "apple"
  },
  {
    "_id": "632c7ad3a06bbd065ecc268a",
    "index": 11,
    "age": 40,
    "isActive": true,
    "eyeColor": "green",
    "name": "Josefina Mendez",
    "gender": "female",
    "company": "Pelangi",
    "email": "josefinamendez@telpod.com",
    "phone": "+1 (973) 473-2797",
    "address": "891 Gallatin Place, Reinerton, North Carolina, 6051",
    "registered": "2019-06-30T11:07:42 -07:00",
    "favoriteFruit": "banana"
  },
  {
    "_id": "632c7ad352906da96167f993",
    "index": 12,
    "age": 30,
    "isActive": false,
    "eyeColor": "brown",
    "name": "Fisher Golden",
    "gender": "male",
    "company": "Intel",
    "email": "fishergolden@telpod.com",
    "phone": "+1 (958) 517-3246",
    "address": "315 John Street, Hailesboro, North Dakota, 2349",
    "registered": "2018-07-20T06:36:34 -07:00",
    "favoriteFruit": "apple"
  },
  {
    "_id": "632c7ad36b04a6501207e07e",
    "index": 13,
    "age": 40,
    "isActive": true,
    "eyeColor": "brown",
    "name": "Latasha Keller",
    "gender": "female",
    "company": "Pelangi",
    "email": "latashakeller@telpod.com",
    "phone": "+1 (982) 554-2526",
    "address": "818 Neptune Court, Blandburg, Alaska, 662",
    "registered": "2020-02-15T03:26:48 -07:00",
    "favoriteFruit": "apple"
  },
  {
    "_id": "632c7ad3054a32800d1220da",
    "index": 14,
    "age": 40,
    "isActive": true,
    "eyeColor": "brown",
    "name": "Valdez Solis",
    "gender": "male",
    "company": "Pelangi",
    "email": "valdezsolis@telpod.com",
    "phone": "+1 (958) 547-3540",
    "address": "646 Grand Street, Falconaire, Arizona, 2894",
    "registered": "2021-09-22T02:40:24 -07:00",
    "favoriteFruit": "strawberry"
  },
  {
    "_id": "632c7ad38da93d6d384b7ed4",
    "index": 15,
    "age": 21,
    "isActive": false,
    "eyeColor": "brown",
    "name": "Washington Wong",
    "gender": "male",
    "company": "Intel",
    "email": "washingtonwong@telpod.com",
    "phone": "+1 (845) 440-2054",
    "address": "700 Walker Court, Winston, Hawaii, 1914",
    "registered": "2014-09-02T10:08:45 -07:00",
    "favoriteFruit": "strawberry"
  }
]

module.exports = datasource